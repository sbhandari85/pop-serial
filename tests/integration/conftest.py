import pytest


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    hub.pop.sub.add(dyne_name="serial")

    yield hub
